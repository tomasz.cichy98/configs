# Configs

My configs for zsh, git, PyCharm...

## Other configs

Terminal font: https://github.com/romkatv/dotfiles-public/blob/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20Regular.ttf  
How to change Windows terminal colors: https://docs.microsoft.com/en-us/windows/terminal/customize-settings/color-schemes  
Intellij settings: https://www.jetbrains.com/help/pycharm/sharing-your-ide-settings.html#import-export-settings  

## Linux terminal config

Unpack `.oh-my-zsh.zip` in `$HOME` (you should have `$HOME/.oh-my-zsh/...` after that) and compy the configs (minimum):
1. `.bashrc`
1. `.zshrc`
1. `.p10k.zsh`

or, to install:

1. Install zsh: https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH
1. Instal oh-my-zsh: https://github.com/ohmyzsh/ohmyzsh
1. Install p10k: https://github.com/romkatv/powerlevel10k#oh-my-zsh

Copy relevant config files from the repo.
